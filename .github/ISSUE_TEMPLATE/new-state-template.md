---
name: New State Template
about: Entry for new state
title: 'New State: [ENTER NAME HERE]'
labels: new state
assignees: ''

---

**State Name:** [Enter State Name]
**Bulletin link:** [Link](#)

### Defaults 

+ [ ] Data extraction done | [HowTo](https://github.com/IBM/covid19-india-data/wiki/Adding-a-new-state-to-the-data-extraction-pipeline)
+ [ ] Added entry into landing page | [HowTo](https://github.com/IBM/covid19-india-data/tree/main/frontend#adding-a-new-page)
+ [ ] Added DB Schema in the Wiki | [Wiki](https://github.com/IBM/covid19-india-data/wiki)

### State-specific stuff

[ADD DESCRIPTION HERE]
